if [ "$(ls /etc/sudoers.d |grep saret)" != "saret" ]; then
    sudo tee /etc/sudoers.d/saret <<EOF
saret	ALL=(ALL) NOPASSWD:ALL
EOF
fi
if [ "$(ls /home |grep saret)" != "saret" ]; then
    sed -i "s/main$/main contrib non-free-firmware/g" /etc/apt/sources.list
    sudo apt -y update
    sudo apt -y install wget nano curl zsh git lsb-release htop apt-transport-https xorg slim ncdu sshpass parallel fish btop screen
    passwd root -d
    username=saret
    password=qwerty
    if [[ $(ip a |grep -oP '(?<=inet\s)\d[^\s]+') =~ '192.168.1.17' ]]; then
        for loc in $(sudo grep -ilr $HOSTNAME /etc); do
            sudo sed -i "s/$HOSTNAME/ToyServer/g" $loc
        done
    fi
    sudo adduser --gecos "" --disabled-password $username
    sudo chpasswd <<<"$username:$password"
    sudo usermod -aG sudo $username
    mkdir /SSH
    chown $username:$username /SSH -R
    chmod au+xwr /SSH -R
    mkdir -p /home/saret/.ssh 2>/dev/null
    curl -sSL -X 'GET' 'https://vault.saret.tk/v1/kv/data/PublicKeys' -H 'accept: application/json' -H 'X-Vault-Token: hvs.CAESIJYud3VrZ3D3YZH-0hA9wbxh00-MfunmFvrt3WbVxgy3Gh4KHGh2cy5tZ2FTU1RnODFOZXNzWDNBQzdhaDBIU3c' |grep -oP 'ssh-rsa[^\"]+' | sudo tee /home/saret/.ssh/authorized_keys 
    chown saret:saret /home/saret/.ssh -R
    # Debian Machine
    sudo reboot
    su $username
fi
if [ "$(ls /etc |grep oh-my-zsh)" = "oh-my-zsh" ]; then
sudo git clone https://github.com/zsh-users/zsh-autosuggestions  /etc/oh-my-zsh/plugins/zsh-autosuggestions
sed -i "s/plugins=(/plugins=\(zsh-autosuggestions z /" ~/.zshrc
sed -i "s/oh-my-zsh.sh/oh-my-zsh.sh\nsource \/etc\/oh-my-zsh\/plugins\/zsh-autosuggestions\/zsh-autosuggestions.zsh/" ~/.zshrc
elif [ "$(ls $HOME |grep oh-my-zsh)" = ".oh-my-zsh" ]; then
git clone https://github.com/zsh-users/zsh-autosuggestions  ~/.oh-my-zsh/plugins/zsh-autosuggestions
sed -i "s/plugins=(/plugins=\(zsh-autosuggestions z /" ~/.zshrc
sed -i "s/oh-my-zsh.sh/oh-my-zsh.sh\nsource ~\/.oh-my-zsh\/custom\/plugins\/zsh-autosuggestions\/zsh-autosuggestions.zsh/" ~/.zshrc
else
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" --unattended 
git clone https://github.com/zsh-users/zsh-autosuggestions  ~/.oh-my-zsh/plugins/zsh-autosuggestions
sed -i "s/plugins=(/plugins=\(zsh-autosuggestions z /" ~/.zshrc
sed -i "s/oh-my-zsh.sh/oh-my-zsh\nsource ~\/.oh-my-zsh\/custom\/plugins\/zsh-autosuggestions\/zsh-autosuggestions.zsh/" ~/.zshrc
fi    
#sudo tee /etc/apt/sources.list.d/notaesalexp.list<<EOF
#deb https://notesalexp.org/tesseract-ocr5/$(lsb_release -cs)/ $(lsb_release -cs) main
#EOF
tee /home/saret/.screenrc <<EOF
# Enable mouse scrolling and scroll bar history scrolling
termcapinfo xterm* ti@:te@
EOF
sudo apt -y update
mkdir /home/saret/.config
mkdir /home/saret/.config/fish
tee /home/saret/.config/fish/fish_variables <<EOF
# This file contains fish universal variable definitions.
# VERSION: 3.0
SETUVAR __fish_initialized:3100
SETUVAR _fish_abbr_fuck:sudo\x20apt\x20\x2dy\x20install
SETUVAR _fish_abbr_go:sudo\x20apt\x20\x2dy
SETUVAR _fish_abbr_no:sudo\x20\x2ds
SETUVAR _fish_abbr_s_21_:ssh\x20Libre
SETUVAR fish_color_autosuggestion:555\x1ebrblack
SETUVAR fish_color_cancel:\x2dr
SETUVAR fish_color_command:005fd7
SETUVAR fish_color_comment:990000
SETUVAR fish_color_cwd:green
SETUVAR fish_color_cwd_root:red
SETUVAR fish_color_end:009900
SETUVAR fish_color_error:ff0000
SETUVAR fish_color_escape:00a6b2
SETUVAR fish_color_history_current:\x2d\x2dbold
SETUVAR fish_color_host:normal
SETUVAR fish_color_host_remote:yellow
SETUVAR fish_color_match:\x2d\x2dbackground\x3dbrblue
SETUVAR fish_color_normal:normal
SETUVAR fish_color_operator:00a6b2
SETUVAR fish_color_param:00afff
SETUVAR fish_color_quote:999900
SETUVAR fish_color_redirection:00afff
SETUVAR fish_color_search_match:bryellow\x1e\x2d\x2dbackground\x3dbrblack
SETUVAR fish_color_selection:white\x1e\x2d\x2dbold\x1e\x2d\x2dbackground\x3dbrblack
SETUVAR fish_color_status:red
SETUVAR fish_color_user:brgreen
SETUVAR fish_color_valid_path:\x2d\x2dunderline
SETUVAR fish_greeting:Welcome\x20to\x20fish\x2c\x20the\x20friendly\x20interactive\x20shell\x0aType\x20\x60help\x60\x20for\x20instructions\x20on\x20how\x20to\x20use\x20fish
SETUVAR fish_key_bindings:fish_default_key_bindings
SETUVAR fish_pager_color_completion:\x1d
SETUVAR fish_pager_color_description:B3A06D\x1eyellow
SETUVAR fish_pager_color_prefix:white\x1e\x2d\x2dbold\x1e\x2d\x2dunderline
SETUVAR fish_pager_color_progress:brwhite\x1e\x2d\x2dbackground\x3dcyan
EOF
sudo  apt install -y $(apt-cache search tesseract-ocr   | awk '{print $1}')
sudo apt -y install ocrmypdf
sudo apt -y install python3-pip
sudo apt -y install jbig2enc
sudo apt -y install jbig2dec
sudo apt -y install imagemagick
sudo mkdir /usr/share/tesseract-ocr/5/tessdata
wget https://raw.githubusercontent.com/tesseract-ocr/tessdata/main/deu_frak.traineddata -O /usr/share/tesseract-ocr/5/tessdata/deu_frak.traineddata
sed -i 's/ZSH_THEME="mrtazz"/ZSH_THEME="agnoster"/' ~/.zshrc
wget https://dot.net/v1/dotnet-install.sh
chmod a+x dotnet-install.sh
./dotnet-install.sh
# sudo chsh $USER -s $(whereis fish)
sudo rm $(ls)
sudo tee /etc/timezone <<<Asia/Jerusalem
git commit --global user.email "saretbenny@gmail.com"
git commit --global user.name "1kamma"
git config --global user.email "saretbenny@gmail.com"
git config --global user.name "1kamma"
if [ "$HOST" = "tritium-h5" || "$HOST" = "libre-computer" ]; then
    curl -sSL "https://www.googleapis.com/drive/v3/files/12E1DAwQaQUT7lWOs1EhTD5Xk_sAnGqjo?alt=media&key=AIzaSyCy8ppkVWYQFSznbe1SHuAITSZ-ux_ZCZk" >/home/saret/.ssh/LibreGithub
    curl -fsSL https://archive.heckel.io/apt/pubkey.txt | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/archive.heckel.io.gpg
    tee /etc/apt/sources.list.d/archive.heckel.io.list <<EOF
deb https://archive.heckel.io/apt debian main
deb-src https://archive.heckel.io/apt debian main
EOF
    sudo apt update
    sudo apt install ntfy
    sudo systemctl enable ntfy
    sudo systemctl start ntfy
    sudo tee /Scripts/rebooting <<EOF
#!/bin/bash
while [[ ! \$(hostname -I) ]]; do
        sleep 1
done
myip=\$(ip a |grep -oP '(?<=inet6\s)2[^/]+')
modemip=\$(sshpass -p '1234567890' ssh admin@192.168.0.1 ip a |grep -oP '(?<=inet6\s)2[^\/]+')
ntfy pub -t "started up Libre server (The TOY-server)" -m "\$(date)\nmy ip: \$myip\n\nmodem ip:\$modemip" https://ntfy.d.metallum.eu.org/ServerState
EOF
    sudo chmod a+x /Scripts/rebooting
    tee /home/saret/.ssh/config <<EOF
Host GitHub
        Hostname ssh.github.com
        Port 443
        User git
        IdentityFile ~/.ssh/LibreGithub

Host gist
        Hostname gist.github.com
        User git
        IdentityFile ~/.ssh/LibreGithub

Host oracle
        HostName xmpp.saret.tk
        User saret
        IdentityFile ~/.ssh/LibreSSH

Host neworacle
        HostName files.saret.tk
        User saret
        IdentityFile ~/.ssh/LibreSSH
EOF
    sudo tee /etc/systemd/system/RunSSH.service <<EOF
[Unit]
Description=Run This SSH in ssh-j server
After=network.target

[Service]
Type=simple
Restart=always
RestartSec=3600
ExecStart=/Scripts/ssh.sh
User=saret

[Install]
WantedBy=multi-user.target
EOF
    sudo chmod a+x /etc/systemd/system/RunSSH.service
    sudo mkdir /Scripts
    sudo tee /Scripts/ssh.sh <<EOF
if [[ "\$(ps -x |grep saret@ssh |awk '{print \$9}')" != *"libre-computer:22:localhost:22"* ]]; then
        ssh saret@ssh-j.com -N -R libre-computer:22:localhost:22
fi
EOF
    sudo tee /Scripts/update_data <<EOF
#!/bin/bash
apt -y update
EOF
    sudo chmod a+x /Scripts/update_data
    sudo tee /nan <<EOF
# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').
#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
# m h  dom mon dow   command

0 4 * * * /Scripts/update_data
@reboot /Scripts/rebooting
EOF
    sudo crontab /nan
cat <<EOF >> .zshrc
#My alias
alias fuck="sudo apt install -y"
alias go="sudo apt -y"
alias no="sudo -s"
alias dl="youtube-dl  -f '(mp4,mkv)bestvideo[height>=720]+bestaudio/bestvideo[height>=720]' -o '%(title)s'"
alias DL="youtube-dl  -f '(mp4,mkv)bestvideo[height>720]+bestaudio/bestvideo[height>720]' -o '%(title)s'"
export DOTNET_ROOT=\$HOME/.dotnet
export PATH=\$PATH:\$HOME/.dotnet
EOF
cat <<EOF >> .bashrc
#My alias
alias fuck="sudo apt install -y"
alias go="sudo apt -y"
alias no="sudo -s"
alias dl="youtube-dl  -f '(mp4,mkv)bestvideo[height>=720]+bestaudio/bestvideo[height>=720]' -o '%(title)s'"
alias DL="youtube-dl  -f '(mp4,mkv)bestvideo[height>720]+bestaudio/bestvideo[height>720]' -o '%(title)s'"
export DOTNET_ROOT=\$HOME/.dotnet
export PATH=\$PATH:\$HOME/.dotnet
EOF
    sudo chmod a+x /Scripts/ssh.sh
    sudo tee /etc/systemd/system/GoogleDrive.service <<EOF
[Unit]
Description=Run Google drive folder, and mount it.
After=network.target

[Service]
Type=simple
ExecStart=google-drive-ocamlfuse ~/GoogleDrive/
User=saret

[Install]
WantedBy=multi-user.target
EOF
    sudo chmod a+x /etc/systemd/system/GoogleDrive.service
    sudo systemctl enable GoogleDrive
    sudo systemctl enable RunSSH
else
cat <<EOF >> .zshrc
#My alias
alias fuck="sudo apt install -y"
alias go="sudo apt -y"
alias no="sudo -s"
alias dl="youtube-dl  -f '(mp4,mkv)bestvideo[height>=720]+bestaudio/bestvideo[height>=720]' -o '%(title)s'"
alias DL="youtube-dl  -f '(mp4,mkv)bestvideo[height>720]+bestaudio/bestvideo[height>720]' -o '%(title)s'"
alias s!="ssh Libre -X"
export DOTNET_ROOT=\$HOME/.dotnet
export PATH=\$PATH:\$HOME/.dotnet
EOF
cat <<EOF >> .bashrc
#My alias
alias fuck="sudo apt install -y"
alias go="sudo apt -y"
alias no="sudo -s"
alias dl="youtube-dl  -f '(mp4,mkv)bestvideo[height>=720]+bestaudio/bestvideo[height>=720]' -o '%(title)s'"
alias DL="youtube-dl  -f '(mp4,mkv)bestvideo[height>720]+bestaudio/bestvideo[height>720]' -o '%(title)s'"
alias s!="ssh Libre -X"
export DOTNET_ROOT=\$HOME/.dotnet
export PATH=\$PATH:\$HOME/.dotnet
EOF
    sudo curl -sSL "https://www.googleapis.com/drive/v3/files/12Axgo2juZfnriBarFq-yh6-i1RD_6pAX?alt=media&key=AIzaSyCy8ppkVWYQFSznbe1SHuAITSZ-ux_ZCZk" -o /SSH/DebianGithub
    sudo chown saret:saret /SSH -R
    sudo chmod 600 /SSH -R
#    sudo sed -i '$a\'"\n\x64\x65\x62\x20\x68\x74\x74\x70\x73\x3a\x2f\x2f\x6e\x6f\x74\x65\x73\x61\x6c\x65\x78\x70\x2e\x6f\x72\x67\x2f\x64\x65\x62\x69\x61\x6e\x2f\x62\x75\x6c\x6c\x73\x65\x79\x65\x2f\x20\x62\x75\x6c\x6c\x73\x65\x79\x65\x20\x6d\x61\x69\x6e\x20\x63\x6f\x6e\x74\x72\x69\x62\x20\x6e\x6f\x6e\x2d\x66\x72\x65\x65\\n\x64\x65\x62\x2d\x73\x72\x63\x20\x68\x74\x74\x70\x73\x3a\x2f\x2f\x6e\x6f\x74\x65\x73\x61\x6c\x65\x78\x70\x2e\x6f\x72\x67\x2f\x64\x65\x62\x69\x61\x6e\x2f\x62\x75\x6c\x6c\x73\x65\x79\x65\x2f\x20\x62\x75\x6c\x6c\x73\x65\x79\x65\x20\x6d\x61\x69\x6e\x20\x63\x6f\x6e\x74\x72\x69\x62\x20\x6e\x6f\x6e\x2d\x66\x72\x65\x65" /etc/apt/sources.list
fi
